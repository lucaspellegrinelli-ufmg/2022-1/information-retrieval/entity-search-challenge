import pandas as pd
import pyterrier as pt
from pyterrier.measures import *
import gensim.downloader as gensim_api
from pyterrier_bert.pyt_cedr import CEDRPipeline
if not pt.started(): pt.init()

from fileutils import read_queries, read_qrels, split_query_qrel

CEDR_MODELS = [
    "vanilla_bert",
    "cedr_knrm",
    "cedr_pacrr",
    "cedr_drmm"
]

# Load computed index
indexref = pt.IndexRef.of("/mnt/d/index/data.properties")
index = pt.IndexFactory.of(indexref)

# Loading datasets
topics = read_queries("data/train_queries.csv")
qrels = read_qrels("data/train_qrels.csv")
kaggle = read_queries("data/test_queries.csv")
train_topics, val_topics, train_qrels, val_qrels = split_query_qrel(topics, qrels, test_size=0.3)

# Training CEDR
def train_cedr(modelname):
    CEDR_body = CEDRPipeline(doc_attr="text", model_out_dir=modelname, modelname=modelname, max_valid_rank=100)
    pipe = pt.BatchRetrieve(index, metadata=["docno", "text"]) >> CEDR_body
    pipe.fit(train_topics, train_qrels, val_topics, val_qrels)

for cedrmodelname in CEDR_MODELS:
    train_cedr(modelname=cedrmodelname)

def BASELINE():
    DPH_br = pt.BatchRetrieve(index, wmodel="DPH")
    TF_IDF = pt.BatchRetrieve(index, wmodel="TF_IDF")
    RETR = pt.BatchRetrieve(index)
    pipeline = RETR >> (TF_IDF ** DPH_br)
    pipeline.compile()
    return pipeline >> pt.apply.doc_score(lambda df: np.prod(df["features"]))

def SINGLE_CEDR(modelname):
    CEDR_body = CEDRPipeline(doc_attr="text", model_out_dir=modelname, modelname=modelname, max_valid_rank=100)
    CEDR_body.load(os.path.join(modelname, "weights.p"))
    RETR = pt.BatchRetrieve(index, metadata=["docno", "text"])
    return RETR >> CEDR_body

rankers = [
    (BASELINE(), "BASELINE")
]

for cedrmodelname in CEDR_MODELS:
    rankers.append((SINGLE_CEDR(modelname=cedrmodelname), cedrmodelname))

print(pt.Experiment(
    retr_systems=[func for func, name in rankers],
    topics=val_topics,
    qrels=val_qrels,
    eval_metrics=[nDCG, nDCG@100],
    names=[name for func, name in rankers],
    verbose=True
))

# model = BASELINE()
# result = pd.DataFrame({"qid": [], "docno": []})
# for index, row in kaggle.iterrows():
#     df = model.search(row["query"]).head(100)
#     df["qid"] = row["qid"]
#     result = pd.concat([result, df[["qid", "docno"]]], ignore_index=True)
# result["QueryId"] = result["qid"]
# result["EntityId"] = result["docno"]
# result[["QueryId", "EntityId"]].to_csv("result.csv", index=False)