import json
import pandas as pd

def read_queries(filename):
    topics = pd.read_csv(filename, dtype={ "QueryId": str, "Query": str })
    topics["qid"] = topics["QueryId"]
    topics["query"] = topics["Query"]
    del topics["QueryId"]
    del topics["Query"]
    topics["query"] = topics["query"].str.replace(r"\'", "", regex=True)
    return topics

def read_qrels(filename):
    qrels = pd.read_csv(filename, dtype={ "QueryId": str, "EntityId": str })
    qrels["qid"] = qrels["QueryId"]
    qrels["docno"] = qrels["EntityId"]
    qrels["label"] = qrels["Relevance"]
    del qrels["QueryId"]
    del qrels["EntityId"]
    del qrels["Relevance"]
    return qrels

def split_query_qrel(queries, qrels, test_size=0.3):
    train_size = 1.0 - test_size
    train_topics = queries.iloc[:int(len(queries) * train_size)]
    train_qrels = qrels[qrels["qid"].isin(train_topics["qid"])]
    val_topics = queries.iloc[int(len(queries) * train_size):]
    val_qrels = qrels[qrels["qid"].isin(val_topics["qid"])]
    return train_topics, val_topics, train_qrels, val_qrels
