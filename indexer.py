import json
import numpy as np
import pyterrier as pt
if not pt.started(): pt.init()

def iter_file(filename, count=None):
    with open(filename, "rt") as file:
        for i, line in enumerate(file):
            if count is not None and i > count: break
            if i % 100000 == 0: print(i)
            json_item = json.loads(line)
            yield {
                "docno" : json_item["id"],
                "text" : json_item["text"]
            }

# Indexing corpus
indexer = pt.IterDictIndexer(
    index_path="./index",
    meta=["docno", "text"],
    meta_lengths=[20, 4096],
    threads=4,
    verbose=True
)

indexref = indexer.index(it=iter_file("data/corpus.jsonl"))
index = pt.IndexFactory.of(indexref)