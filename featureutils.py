import numpy as np
from nltk import word_tokenize

def word_to_vec(word, w2v_model):
    return w2v_model[word] if ord in w2v_model else np.random.rand(50)

def document_to_vec(text, w2v_model):
    doc_tokens = word_tokenize(str(text))
    if len(doc_tokens) == 0: return np.zeros(50)
    present_perms = [tok for tok in doc_tokens if tok in w2v_model]
    embeddings = w2v_model[present_perms] if len(present_perms) > 0 else np.zeros((0, 50))
    missing_embeddings = np.random.rand(len(doc_tokens) - len(present_perms), 50)
    full_embeddings = np.concatenate((embeddings, missing_embeddings), axis=0)
    word2vec = np.mean(full_embeddings, axis=0)
    return word2vec

def keyword_expander(doc):
    def keyword_to_list(keyword_str):
        try:
            keywords = json.loads(keyword_str)
        except:
            keywords = keyword_str[1:].split(", ")
            keywords = [x.replace("\"", "") for x in keywords]

        if len(keywords) > 0: return " ".join(keywords)
        return "something"
    
    doc["plain_keywords"] = doc["keywords"].apply(keyword_to_list)
    return doc